//
//  DemosDataModel.swift
//  iOSDemos
//
//  Created by Akshay Phadke on 07/01/22.
//

import Foundation

struct DemosDataModel: Codable {
    
    enum Keys: String, CaseIterable {
        case demoSectionTitle
        case demos
    }
    
    enum DemosSections: String, CaseIterable {
        case uiTableView = "UITableView"
        case uiCollectionView = "UICollectionView"
        case concurrency = "Concurrency"
        
        enum DemoSectionsIndexes: Int {
            case uiTableView
            case uiCollectionView
            case concurrency
        }
        
        enum UITableViewDemos: String, CaseIterable {
            case protoTypeCells = "Prototype Cells"
            case subtitleCells = "Subtitle Cells"
            case customCells = "Custom Cells"
            
            enum UITableViewDemosIndexes: Int, CaseIterable {
                case protoTypeCells
                case subtitleCells
                case customCells
            }
        }
        
        enum UICollectionViewDemos: String, CaseIterable {
            case simpleFlowLayout = "Simple Flow Layout"
            
            enum UICollectionViewDemosIndexes: Int, CaseIterable {
                case simpleFlowLayout
            }
        }
        
        enum ConcurrencyDemos: String, CaseIterable {
            case asyncAwait = "Async-Await"
            
            enum ConcurrencyDemosIndexes: Int, CaseIterable {
                case asyncAwait
            }
        }
    }
    
    static func demos() -> [[String: Any]] {
        return [
            [Keys.demoSectionTitle.rawValue: DemosSections.uiTableView.rawValue,
             Keys.demos.rawValue: [DemosSections.UITableViewDemos.protoTypeCells.rawValue,
                                   DemosSections.UITableViewDemos.subtitleCells.rawValue,
                                   DemosSections.UITableViewDemos.customCells.rawValue]
            ],
            [Keys.demoSectionTitle.rawValue: DemosSections.uiCollectionView.rawValue,
             Keys.demos.rawValue: [DemosSections.UICollectionViewDemos.simpleFlowLayout.rawValue]
            ],
            [Keys.demoSectionTitle.rawValue: DemosSections.concurrency.rawValue,
             Keys.demos.rawValue: [DemosSections.ConcurrencyDemos.asyncAwait.rawValue]
            ]
        ]
    }
}
