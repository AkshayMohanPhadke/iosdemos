//
//  PrototypeAndSubtitleCellsDataModel.swift
//  iOSDemos
//
//  Created by Akshay Phadke on 10/01/22.
//

import Foundation

struct PrototypeAndSubtitleCellsDataModel: Codable {
    enum Keys: String, CaseIterable {
        case sectionTitle
        case demo
        case data
        case labelText
        case imageName
        case titleText
        case subtitleText
    }
    
    enum DemosSections: String, CaseIterable {
        case oneLabelPrototypeCell = "Prototype Cell With One Expanding Label"
        case oneImageAndLabelPrototypeCell = "Prototype Cell With One Expanding Label And An Image"
        case subtitleCell = "A Cell With Title And Subtitle"
        
        enum DemoSectionsIndexes: Int {
            case oneLabelPrototypeCell
            case oneImageAndLabelPrototypeCell
            case subtitleCell
        }
        
        enum OneLabelPrototypeCell: String, CaseIterable {
            case labelText = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
            
            enum OneLabelPrototypeCellIndexes: Int, CaseIterable {
                case labelText
            }
        }
        
        enum OneImageAndLabelPrototypeCell: String, CaseIterable {
            case labelText = "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc."
            case imageName = "temp"
            
            enum OneImageAndLabelPrototypeCellIndexes: Int, CaseIterable {
                case labelText
            }
        }
        
        enum SubtitleCell: String, CaseIterable {
            case titleText = "Slackbot'"
            case subtitleText = "This is your space. Draft messages, list your to-dos, or keep links and files handy. You can also talk to yourself here, but please bear in mind you’ll have to supply both sides of the conversation."
            
            enum SubtitleCellIndexes: Int, CaseIterable {
                case titleText
            }
        }
    }
    
    static func data() -> [[String: Any]] {
        return [
            [Keys.sectionTitle.rawValue: DemosSections.oneLabelPrototypeCell.rawValue,
             Keys.data.rawValue: [Keys.labelText.rawValue: DemosSections.OneLabelPrototypeCell.labelText.rawValue]
            ],
            [Keys.sectionTitle.rawValue: DemosSections.oneImageAndLabelPrototypeCell.rawValue,
             Keys.data.rawValue: [Keys.labelText.rawValue: DemosSections.OneImageAndLabelPrototypeCell.labelText.rawValue,
                                  Keys.imageName.rawValue: ""]
            ],
            [Keys.sectionTitle.rawValue: DemosSections.subtitleCell.rawValue,
             Keys.data.rawValue: [Keys.titleText.rawValue: DemosSections.SubtitleCell.titleText.rawValue,
                                  Keys.subtitleText.rawValue: DemosSections.SubtitleCell.subtitleText.rawValue]
            ]
        ]
    }
}
