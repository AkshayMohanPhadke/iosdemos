//
//  ProtoTypeAndSubtitleCellsViewController.swift
//  iOSDemos
//
//  Created by Akshay Phadke on 10/01/22.
//

import UIKit

class ProtoTypeAndSubtitleCellsViewController: UIViewController {

    enum CellIdentifiers {
        static let oneLabelPrototypeCellIdentifier = "OneLabelPrototypeCell"
    }
    
    var data = PrototypeAndSubtitleCellsDataModel.data()
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupTableView()
    }
    
    //MARK:- Helper Methods
    func setupView() {
        navigationItem.largeTitleDisplayMode = .never
    }
    
    func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(OneLabelPrototypeCell.self, forCellReuseIdentifier: CellIdentifiers.oneLabelPrototypeCellIdentifier)
    }
}

//MARK:- UITableViewDataSource
extension ProtoTypeAndSubtitleCellsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.oneLabelPrototypeCellIdentifier, for: indexPath) as! OneLabelPrototypeCell
            return cell
        }
        else {
            return UITableViewCell()
        }
    }
}

//MARK:- UITableViewDelegate
extension ProtoTypeAndSubtitleCellsViewController: UITableViewDelegate {
    
}

