//
//  ViewController.swift
//  iOSDemos
//
//  Created by Akshay Phadke on 06/01/22.
//

import UIKit

class DemoListViewController: UIViewController {

    enum CellIdentifiers {
        static let demoCellIdentifier = "DemoCell"
    }
    
    let demos = DemosDataModel.demos()
    
    @IBOutlet weak var demosTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 13.0, *) {
            let navBarAppearance = UINavigationBarAppearance()
            navBarAppearance.configureWithOpaqueBackground()
            navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
            navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
            navBarAppearance.backgroundColor = .systemGray2
            navigationController?.navigationBar.standardAppearance = navBarAppearance
            navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
        }
        
        setupDemosTableView()
    }

    //MARK:- Helper Methods
    func setupDemosTableView() {
        demosTableView.dataSource = self
        demosTableView.delegate = self
        demosTableView.register(UITableViewCell.self, forCellReuseIdentifier: CellIdentifiers.demoCellIdentifier)
    }
    
    func navigate(forIndexPath indexPath: IndexPath, withValue value: Any) {
        switch indexPath.section {
        case DemosDataModel.DemosSections.DemoSectionsIndexes.concurrency.rawValue:
            switch (indexPath.row) {
                case DemosDataModel.DemosSections.ConcurrencyDemos.ConcurrencyDemosIndexes.asyncAwait.rawValue:
                break
            default:
                print("N/A")
            }
        break
            
        case DemosDataModel.DemosSections.DemoSectionsIndexes.uiCollectionView.rawValue:
            switch (indexPath.row) {
            case DemosDataModel.DemosSections.UICollectionViewDemos.UICollectionViewDemosIndexes.simpleFlowLayout.rawValue:
                break
            default:
                print("N/A")
            }
        break
            
        case DemosDataModel.DemosSections.DemoSectionsIndexes.uiTableView.rawValue:
            switch (indexPath.row) {
            case DemosDataModel.DemosSections.UITableViewDemos.UITableViewDemosIndexes.customCells.rawValue:
                    break
            case DemosDataModel.DemosSections.UITableViewDemos.UITableViewDemosIndexes.protoTypeCells.rawValue,
                DemosDataModel.DemosSections.UITableViewDemos.UITableViewDemosIndexes.subtitleCells.rawValue:
                
                guard let viewController = storyboard?.instantiateViewController(withIdentifier: Constants.StoryboardIdentifiers.protoTypeAndSubtitleCellsViewController.rawValue) as? ProtoTypeAndSubtitleCellsViewController else { return }
                navigationController?.pushViewController(viewController, animated: true)
                    break
            default:
                print("N/A")
            }
            
        default:
            print("N/A")
        }
    }
}

//MARK:- UITableViewDataSource
extension DemoListViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return demos.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (demos[section][DemosDataModel.Keys.demos.rawValue] as! Array<Any>).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.demoCellIdentifier, for: indexPath)
        cell.textLabel?.text = (demos[indexPath.section][DemosDataModel.Keys.demos.rawValue] as! Array<String>)[indexPath.row]
        cell.textLabel?.numberOfLines = 0
        return cell
    }
}

//MARK:- UITableViewDelegate
extension DemoListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true);
        navigate(forIndexPath: indexPath, withValue: demos[indexPath.section])
    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 50))
//        view.backgroundColor = .lightGray
//        return view
//    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return (demos[section][DemosDataModel.Keys.demoSectionTitle.rawValue] as! String)
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
}


