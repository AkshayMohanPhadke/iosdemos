//
//  Constants.swift
//  iOSDemos
//
//  Created by Akshay Phadke on 10/01/22.
//

import Foundation

class Constants {
    enum StoryboardIdentifiers: String {
        case protoTypeAndSubtitleCellsViewController = "ProtoTypeAndSubtitleCellsViewController"
    }
}
